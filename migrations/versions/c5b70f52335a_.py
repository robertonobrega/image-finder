"""Remove body from service_tokens

Revision ID: c5b70f52335a
Revises: 6d7bb3b7952e
Create Date: 2022-02-12 04:28:12.637158

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c5b70f52335a'
down_revision = '6d7bb3b7952e'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('service_tokens', schema=None) as batch_op:
        batch_op.drop_column('body')


def downgrade():
    with op.batch_alter_table('service_tokens', schema=None) as batch_op:
        batch_op.add_column(
            sa.Column('body', sa.VARCHAR(length=255), nullable=False)
        )
