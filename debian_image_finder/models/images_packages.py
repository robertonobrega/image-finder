from debian_image_finder.extensions.database import db


images_packages = db.Table(
    'images_packages',
    db.Column(
        'image_id',
        db.Integer,
        db.ForeignKey('images.id'),
        primary_key=True
    ),
    db.Column(
        'package_id',
        db.Integer,
        db.ForeignKey('packages.id'),
        primary_key=True
    )
)
