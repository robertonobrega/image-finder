from debian_image_finder.extensions.serialization import ma
from debian_image_finder.models.package import Package


class PackageSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Package
        load_instance = True


package_schema = PackageSchema()
packages_schema = PackageSchema(many=True)
