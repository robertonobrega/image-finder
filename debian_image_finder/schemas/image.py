from debian_image_finder.extensions.serialization import ma
from debian_image_finder.models.image import Image


class ImageSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Image
        load_instance = True
        include_fk = True


image_schema = ImageSchema(exclude=("id", "created_at",))
images_schema = ImageSchema(exclude=("id", "created_at",), many=True)
