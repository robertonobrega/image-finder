from flask_simplemde import SimpleMDE

mde = SimpleMDE()


def init_app(app):
    mde.init_app(app)
