from flask_seeder import FlaskSeeder
from debian_image_finder.extensions.database import db

seed = FlaskSeeder()


def init_app(app):
    seed.init_app(app, db)
