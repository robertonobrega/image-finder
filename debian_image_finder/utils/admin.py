from flask import redirect, url_for, flash
from flask_login import current_user
from functools import wraps


def admin_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if current_user and not current_user.admin:
            flash('You do not have access to this page.', category='error')
            return redirect(url_for('.index'))
        return f(*args, **kwargs)

    return decorated
