from debian_image_finder.tests.models.base import TestBase
from debian_image_finder.models.provider import Provider


class TestProviderModel(TestBase):

    def test_provider_count(self):
        """
        Test number of records in Provider table.
        """
        self.assertEqual(Provider.query.count(), 1)

    def test_create_provider(self):
        """
        Test create provider.
        """
        provider = Provider(
            name='Azure',
            vendor='azure',
            description='',
            markdown=''
        )
        provider.save_to_db()

        retrieved_provider = Provider.find_by_id(provider.id)

        self.assertEqual(Provider.query.count(), 2)
        self.assertEqual(retrieved_provider.id, provider.id)
        self.assertEqual(retrieved_provider.name, provider.name)
        self.assertEqual(retrieved_provider.vendor, provider.vendor)
        self.assertEqual(retrieved_provider.description, provider.description)
        self.assertEqual(retrieved_provider.markdown, provider.markdown)

    def test_find_provider_by_id(self):
        """
        Test find provider by id.
        """
        retrieved_provider = Provider.find_by_id(self.provider.id)

        self.assertEqual(Provider.query.count(), 1)
        self.assertEqual(retrieved_provider.id, self.provider.id)
        self.assertEqual(retrieved_provider.name, self.provider.name)
        self.assertEqual(retrieved_provider.vendor, self.provider.vendor)
        self.assertEqual(retrieved_provider.description, self.provider.description)
        self.assertEqual(retrieved_provider.markdown, self.provider.markdown)

    def test_find_provider_by_name(self):
        """
        Test find provider by name.
        """
        retrieved_provider = Provider.find_by_name(self.provider.name)

        self.assertEqual(Provider.query.count(), 1)
        self.assertEqual(retrieved_provider.id, self.provider.id)
        self.assertEqual(retrieved_provider.name, self.provider.name)
        self.assertEqual(retrieved_provider.vendor, self.provider.vendor)
        self.assertEqual(retrieved_provider.description, self.provider.description)
        self.assertEqual(retrieved_provider.markdown, self.provider.markdown)

    def test_find_provider_by_vendor(self):
        """
        Test find provider by vendor.
        """
        retrieved_provider = Provider.find_by_vendor(self.provider.vendor)

        self.assertEqual(Provider.query.count(), 1)
        self.assertEqual(retrieved_provider.id, self.provider.id)
        self.assertEqual(retrieved_provider.name, self.provider.name)
        self.assertEqual(retrieved_provider.vendor, self.provider.vendor)
        self.assertEqual(retrieved_provider.description, self.provider.description)
        self.assertEqual(retrieved_provider.markdown, self.provider.markdown)

    def test_update_provider(self):
        """
        Test update provider from db.
        """
        data = {
            'name': 'AWS-updated',
            'vendor': 'ec2-updated',
            'description': 'updated',
            'markdown': 'updated'
        }

        self.provider.update(data)
        self.provider.save_to_db()

        retrieved_provider = Provider.find_by_id(self.provider.id)

        self.assertEqual(Provider.query.count(), 1)
        self.assertEqual(retrieved_provider.id, self.provider.id)
        self.assertEqual(retrieved_provider.name, self.provider.name)
        self.assertEqual(retrieved_provider.vendor, self.provider.vendor)
        self.assertEqual(retrieved_provider.description, self.provider.description)
        self.assertEqual(retrieved_provider.markdown, self.provider.markdown)

    def test_delete_provider_from_db(self):
        """
        Test delete provider from db.
        """
        self.provider.delete_from_db()

        self.assertEqual(Provider.query.count(), 0)
