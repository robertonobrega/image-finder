from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField


class PackageSearchForm(FlaskForm):
    package_name = StringField('Name')
    submit = SubmitField('Search')
