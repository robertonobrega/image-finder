from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SubmitField
from wtforms.validators import InputRequired, NumberRange, Length


class ServiceTokenForm(FlaskForm):
    name = StringField(
        'Name',
        default='',
        validators=[
            InputRequired(),
            Length(
                min=5,
                max=20
            )
        ]
    )
    expiration = IntegerField(
        'Expiration',
        default='',
        validators=[
            InputRequired(),
            NumberRange(
                min=1,
                max=365
            )
        ]
    )
    submit = SubmitField('Generate')
