from flask import render_template, request, redirect, url_for
from flask_login import login_required
import markdown2
from debian_image_finder.models.provider import Provider
from debian_image_finder.utils.admin import admin_required


@login_required
@admin_required
def edit_provider(vendor):
    if request.method == 'POST':
        data = {
            'name': request.form['name'],
            'vendor': request.form['vendor'],
            'description': request.form['description'],
            'markdown': request.form['markdown']
        }

        provider = Provider.find_by_vendor(vendor)
        if provider:
            provider.update(data)
            provider.save_to_db()

            return redirect(url_for('.index'))

    provider = Provider.find_by_vendor(vendor)
    content = markdown2.markdown(provider.markdown)

    if not provider:
        return redirect(url_for('.index'))

    return render_template('edit_provider.html',
                           provider=provider,
                           content=content)
