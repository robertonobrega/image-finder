import json
import logging
from flask_seeder import Seeder
from dynaconf import settings
from os import listdir
from os.path import isfile, join
from debian_image_finder.utils.publish import publish_images


class ImageSeeder(Seeder):

    def __init__(self):
        self.priority = 2

    def run(self):
        if settings.SKIP_IMAGE_SEEDER:
            return

        buster_path = 'seeds/buster'

        for file in listdir(buster_path):
            if isfile(join(buster_path, file)):
                logging.info(f'Seeding file: {file}')
                publish_file = open(f'{buster_path}/{file}')
                data = json.load(publish_file)

                publish_images(data)
